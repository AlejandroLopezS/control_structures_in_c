#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "library.c"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es) 
 * @version 1.0
 */

/////////////////////////////////////////////////////////
// -------------------- maximum ---------------------- //
/////////////////////////////////////////////////////////
void testMaximum_7_5() {
    // Given
    int maximo;
    // When
    maximo = maximum(7,5);
    // Then
	assertEquals_int(7, maximo);
}

void testMaximum_3_9() {
    // Given
    int maximo;
    // When
    maximo = maximum(3,9);
    // Then
	assertEquals_int(9, maximo);
}

void testMaximum_4_4() {
    // Given
    int maximo;
    // When
    maximo = maximum(4,4);
    // Then
	assertEquals_int(4, maximo);
}
	
	
/////////////////////////////////////////////////////////
// -------------------- sumUpTo ---------------------- //
/////////////////////////////////////////////////////////
	
void testSumUpTo_minus1() {
    // Given
    int suma;
    // When
    suma = sumUpTo(-1);
    // Then
	assertEquals_int(0, suma);
}

void testSumUpTo_0() {
    // Given
  int suma;
    // When
    suma = sumUpTo(0);
    // Then
	assertEquals_int(0, suma);
}

void testSumUpTo_1() {
    // Given
int suma;
    // When
    suma = sumUpTo(1);
    // Then
	assertEquals_int(1, suma);
}

void testSumUpTo_3() {
    int suma;
    // When
    suma = sumUpTo(3);
    // Then
	assertEquals_int(6, suma);
}

void testSumUpTo_7() {
    // Given
    int suma;
    // When
    suma = sumUpTo(7);
    // Then
	assertEquals_int(28, suma);
}

	
/////////////////////////////////////////////////////////
// ---------------------- sum ------------------------ //
/////////////////////////////////////////////////////////
	
void testSum_Empty() {
    // Given
    int array[0];
    int suma;
    // When
    suma = sum(array, 0);
    // Then
	assertEquals_Int(0, suma);
}

void testSum_5() {
    int array[] = {5};
    int suma;
    // When
    suma = sum(array, 1);
    // Then
	assertEquals_Int(5, suma);
}

void testSum_5_7() {
  int array[] = {5,7};
    int suma;
    // When
    suma = sum(array, 2);
    // Then
	assertEquals_Int(12, suma);
}

void testSum_3_minus4_8() {
  int array[] = {5, -4, 8};
    int suma;
    // When
    suma = sum(array, 3);
    // Then
	assertEquals_Int(9, suma);
}

void testSum_minus2_5_3_minus1_9() {
    // Given
  int array[] = {-2,5,3,-1,9};
    int suma;
    // When
    suma = sum(array, 5);
    // Then
	assertEquals_Int(14, suma);
}

void testSum_9_minus8_15_6_minus10_7_4() {
    // Given
  int array[] = {9,-8,15,6,-10,7,4};
    int suma;
    // When
    suma = sum(array, 7);
    // Then
	assertEquals_Int(23, suma);
}


