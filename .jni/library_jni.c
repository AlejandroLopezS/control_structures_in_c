#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "library.c"

extern JNIEnv *javaEnv;

JNIEXPORT jint JNICALL Java_library_maximum
  (JNIEnv *env, jobject object, jint left, jint right)
{
    javaEnv = env;
    int c_left = toInt(left);
    int c_right = toInt(right);
    int c_maximum = maximum(c_left, c_right);
    return toJint(c_maximum);
}

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

JNIEXPORT jint JNICALL Java_library_sumUpTo
  (JNIEnv *env, jobject object, jint end)
{
    javaEnv = env;
    int c_end = toInt(end);
    int c_sumUpTo = sumUpTo(c_end);
    return toJint(c_sumUpTo);
}

JNIEXPORT jint JNICALL Java_library_sum
  (JNIEnv *env, jobject object, jintArray value, jint valueLength)
{
    javaEnv = env;
    int c_valueLength = toInt(valueLength);
    int* c_value = toIntArray(value, c_valueLength);
    int c_sum = sum(c_value, c_valueLength);
    return toJint(c_sum);
}
